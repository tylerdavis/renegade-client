class Cookies {

  static get (name) {
    var name = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0)==' ') c = c.substring(1);
      if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
    }
    return undefined;
  }

  static set (name, value, exdays) {
    var exdate = new Date();
    exdate.setDate(exdate.getDate() + exdays);
    value = encodeURI(value) + ((exdays === null) ? "" : "; expires=" + exdate.toUTCString()) + "; path=/";
    document.cookie = name + "=" + value;
  }

  static getOrSetId () {
    if (this.get('_an_id')) {
      return this.get('_an_id');
    } else {
      var text = "";
      var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
      for( var i=0; i < 25; i++ ) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
      }
      this.set('_an_id', text);
      return text;
    }
  }
}

module.exports = Cookies;