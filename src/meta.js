module.exports = function (options) {
  if (!options || !options.name || !options.content) return;
  var element = document.createElement('meta');
  element.name = options.name;
  element.content = options.content;
  var head = document.getElementsByTagName('head')[0];
  head.appendChild(element);
};