var Request    = require('./request');
var xpath      = require('./xpath');
var injectMeta = require('./meta');

class Analytics {

  /**
   * @param options {object}
   * @param options.pageView {boolean}
   * @param options.organization {string}
   * @param options.datastore {string}
   * @param options.tags {object}
   * @param options.fields {object}
   */
  constructor (options) {
    if (!options.organization) throw new Error('You must supply a valid api key.');
    this.map          = {};
    this.organization = options.organization;
    this.tags         = options.tags || {};
    this.fields       = options.fields || {};
    this.client       = new Request({ url : (options.datastore || 'https://rg.tmd.io') });
    injectMeta({ name : 'rg', content : this.organization });

    if (options.canInstrument !== false) this.getEventsMap();

    if (options.pageView !== false) this.pageView();
    document.addEventListener('click', this.hasClicked.bind(this), true);
  }

  /**
   * record event
   * @params options {object}
   * @param options.type {string} type of event
   * @param options.name {string} name of event
   * @param options.tags {object}
   * @param options.fields {object}
   */
  record (options) {

    /**
     * Backward compatibility
     * _rg.record(eventType, eventName, tagsObject);
     */
    if ('string' === typeof options) {
      var type     = options;
      options      = {};
      options.type = type;
      if (!arguments[1]) throw new Error('You must supply an event name.');
      options.name = arguments[1];
      options.tags = arguments[2] || {};
    }

    var requestTags   = {};
    var requestFields = {};

    /** We need to merge all the defaults with anything passed in */
    for (var tag in this.tags) {
      if (this.tags.hasOwnProperty(tag)) {
        requestTags[tag] = this.tags[tag];
      }
    }
    for (var field in this.fields) {
      if (this.fields.hasOwnProperty(field)) {
        requestFields[field] = this.fields[field];
      }
    }
    for (var tagName in options.tags) {
      if (options.tags.hasOwnProperty(tagName)) {
        requestTags[tagName] = options.tags[tagName];
      }
    }
    for (var fieldName in options.fields) {
      if (options.fields.hasOwnProperty(fieldName)) {
        requestFields[fieldName] = options.fields[fieldName];
      }
    }

    this.client.post('/record', {
      organization : this.organization,
      name         : options.name,
      type         : options.type,
      tags         : requestTags,
      fields       : requestFields
    });
  }

  /**
   * Scan the events map and record a registered event if found
   * @param event
   */
  hasClicked (event) {
    window.setTimeout(function () {
      var eventObject = this.map[xpath.createXPathFromElement(event.target)];
      if (eventObject && eventObject.type === 'click') {
        var tags = {
          origin : window.location.origin,
          path   : window.location.pathname,
        };
        for (var tagName in eventObject.tags) {
          if (eventObject.tags.hasOwnProperty(tagName)) {
            tags[tagName] = eventObject.tags[tagName];
          }
        }
        var fields = {
          userAgent : navigator.userAgent
        };
        for (var fieldName in eventObject.fields) {
          if (eventObject.fields.hasOwnProperty(fieldName)) {
            fields[fieldName] = eventObject.fields[fieldName];
          }
        }
        this.record({
          name   : eventObject.name,
          type   : eventObject.type,
          tags   : tags,
          fields : fields
        });
      }
    }.bind(this), 0);
  }

  /**
   * Record the page view.  event name currently getting set to document title
   */
  pageView () {
    var tags = {
      origin : window.location.origin,
      path   : window.location.pathname
    };

    var fields = {
      userAgent : navigator.userAgent,
      referrer  : document.referrer.split('/')[2]
    };

    this.record({
      name   : document.title,
      type   : 'page view',
      tags   : tags,
      fields : fields
    })
  }

  /**
   * Pull event map for this origin/path
   */
  getEventsMap () {
    this.client.get('/page_events', {
      origin : window.location.origin,
      path   : window.location.pathname
    }, function (response) {
      response = JSON.parse(response);
      for (var attr in response) {
        if (response.hasOwnProperty(attr)) {
          this.map[attr] = response[attr];
        }
      }
    }.bind(this));
  }

}

window._rg = function (options, map, defaults) {
  return new Analytics(options, map, defaults);
};