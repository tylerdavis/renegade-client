/**
 * Returns the xpath of an element
 * @param element
 * @returns {String} the xpath string
 */
exports.createXPathFromElement = function (element) {
  var allNodes = document.getElementsByTagName('*');
  for (var segs = []; element && element.nodeType == 1; element = element.parentNode) {
    if (element.hasAttribute('id')) {
      var uniqueIdCount = 0;
      for (var n = 0; n < allNodes.length; n++) {
        if (allNodes[n].hasAttribute('id') && allNodes[n].id == element.id) uniqueIdCount++;
        if (uniqueIdCount > 1) break;
      }
      if (uniqueIdCount == 1) {
        segs.unshift('id("' + element.getAttribute('id') + '")');
        return segs.join('/');
      } else {
        segs.unshift(element.localName.toLowerCase() + '[@id="' + element.getAttribute('id') + '"]');
      }
    } else if (element.hasAttribute('class')) {
      segs.unshift(element.localName.toLowerCase() + '[@class="' + element.getAttribute('class') + '"]');
    } else {
      for (var i = 1, sib = element.previousSibling; sib; sib = sib.previousSibling) {
        if (sib.localName == element.localName)  i++;
      }
      segs.unshift(element.localName.toLowerCase() + '[' + i + ']');
    }
  }
  return segs.length ? '/' + segs.join('/') : null;
};

/**
 * Get an element by xpath
 * @param path
 * @returns {Node}
 */
exports.lookupElementByXPath = function (path) {
  var evaluator = new XPathEvaluator();
  var result    = evaluator.evaluate(path, document.documentElement, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null);
  return result.singleNodeValue;
};