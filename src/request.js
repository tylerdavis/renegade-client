class Request {

  /**
   * @param options {object}
   * @param options.url {string}
   */
  constructor (options) {
    this.url = options.url;
  }

  get (path, parameters, callback) {
    var query = this.url + path;
    if (parameters) {
      query += '?';
      var paramsArray = [];
      for (var key in parameters) {
        if (parameters.hasOwnProperty(key)) {
          paramsArray.push([key, '=', parameters[key]].join(''));
        }
      }
      query += paramsArray.join('&');
    }
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function () {
      if (xmlhttp.readyState == XMLHttpRequest.DONE) {
        if (xmlhttp.status == 200) {
          callback(xmlhttp.responseText);
        }
        else { /** terror */
        }
      }
    };
    xmlhttp.open("GET", query, true);
    xmlhttp.send();
  }

  /**
   * @param path {String}
   * @param parameters {object}
   */
  post (path, parameters) {
    this.xmlhttp = new XMLHttpRequest();
    this.xmlhttp.open("POST", this.url + path, true);
    this.xmlhttp.withCredentials = true;
    this.xmlhttp.setRequestHeader("Content-type", "application/json");
    this.xmlhttp.onreadystatechange = function () {
      if (this.xmlhttp.readyState == 4 && this.xmlhttp.status == 200) {
      }
    }.bind(this);
    this.xmlhttp.send(JSON.stringify(parameters));
  }

}

module.exports = Request;