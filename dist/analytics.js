(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var Request = require('./request');
var xpath = require('./xpath');
var injectMeta = require('./meta');

var Analytics = (function () {

  /**
   * @param options {object}
   * @param options.pageView {boolean}
   * @param options.organization {string}
   * @param options.datastore {string}
   * @param options.tags {object}
   * @param options.fields {object}
   */

  function Analytics(options) {
    _classCallCheck(this, Analytics);

    if (!options.organization) throw new Error('You must supply a valid api key.');
    this.map = {};
    this.organization = options.organization;
    this.tags = options.tags || {};
    this.fields = options.fields || {};
    this.client = new Request({ url: options.datastore || 'https://rg.tmd.io' });
    injectMeta({ name: 'rg', content: this.organization });

    if (options.canInstrument !== false) this.getEventsMap();

    if (options.pageView !== false) this.pageView();
    document.addEventListener('click', this.hasClicked.bind(this), true);
  }

  _createClass(Analytics, [{
    key: 'record',

    /**
     * record event
     * @params options {object}
     * @param options.type {string} type of event
     * @param options.name {string} name of event
     * @param options.tags {object}
     * @param options.fields {object}
     */
    value: function record(options) {

      /**
       * Backward compatibility
       * _rg.record(eventType, eventName, tagsObject);
       */
      if ('string' === typeof options) {
        var type = options;
        options = {};
        options.type = type;
        if (!arguments[1]) throw new Error('You must supply an event name.');
        options.name = arguments[1];
        options.tags = arguments[2] || {};
      }

      var requestTags = {};
      var requestFields = {};

      /** We need to merge all the defaults with anything passed in */
      for (var tag in this.tags) {
        if (this.tags.hasOwnProperty(tag)) {
          requestTags[tag] = this.tags[tag];
        }
      }
      for (var field in this.fields) {
        if (this.fields.hasOwnProperty(field)) {
          requestFields[field] = this.fields[field];
        }
      }
      for (var tagName in options.tags) {
        if (options.tags.hasOwnProperty(tagName)) {
          requestTags[tagName] = options.tags[tagName];
        }
      }
      for (var fieldName in options.fields) {
        if (options.fields.hasOwnProperty(fieldName)) {
          requestFields[fieldName] = options.fields[fieldName];
        }
      }

      this.client.post('/record', {
        organization: this.organization,
        name: options.name,
        type: options.type,
        tags: requestTags,
        fields: requestFields
      });
    }
  }, {
    key: 'hasClicked',

    /**
     * Scan the events map and record a registered event if found
     * @param event
     */
    value: function hasClicked(event) {
      window.setTimeout((function () {
        var eventObject = this.map[xpath.createXPathFromElement(event.target)];
        if (eventObject && eventObject.type === 'click') {
          var tags = {
            origin: window.location.origin,
            path: window.location.pathname
          };
          for (var tagName in eventObject.tags) {
            if (eventObject.tags.hasOwnProperty(tagName)) {
              tags[tagName] = eventObject.tags[tagName];
            }
          }
          var fields = {
            userAgent: navigator.userAgent
          };
          for (var fieldName in eventObject.fields) {
            if (eventObject.fields.hasOwnProperty(fieldName)) {
              fields[fieldName] = eventObject.fields[fieldName];
            }
          }
          this.record({
            name: eventObject.name,
            type: eventObject.type,
            tags: tags,
            fields: fields
          });
        }
      }).bind(this), 0);
    }
  }, {
    key: 'pageView',

    /**
     * Record the page view.  event name currently getting set to document title
     */
    value: function pageView() {
      var tags = {
        origin: window.location.origin,
        path: window.location.pathname
      };

      var fields = {
        userAgent: navigator.userAgent,
        referrer: document.referrer.split('/')[2]
      };

      this.record({
        name: document.title,
        type: 'page view',
        tags: tags,
        fields: fields
      });
    }
  }, {
    key: 'getEventsMap',

    /**
     * Pull event map for this origin/path
     */
    value: function getEventsMap() {
      this.client.get('/page_events', {
        origin: window.location.origin,
        path: window.location.pathname
      }, (function (response) {
        response = JSON.parse(response);
        for (var attr in response) {
          if (response.hasOwnProperty(attr)) {
            this.map[attr] = response[attr];
          }
        }
      }).bind(this));
    }
  }]);

  return Analytics;
})();

window._rg = function (options, map, defaults) {
  return new Analytics(options, map, defaults);
};

},{"./meta":2,"./request":3,"./xpath":4}],2:[function(require,module,exports){
'use strict';

module.exports = function (options) {
  if (!options || !options.name || !options.content) return;
  var element = document.createElement('meta');
  element.name = options.name;
  element.content = options.content;
  var head = document.getElementsByTagName('head')[0];
  head.appendChild(element);
};

},{}],3:[function(require,module,exports){
'use strict';

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var Request = (function () {

  /**
   * @param options {object}
   * @param options.url {string}
   */

  function Request(options) {
    _classCallCheck(this, Request);

    this.url = options.url;
  }

  _createClass(Request, [{
    key: 'get',
    value: function get(path, parameters, callback) {
      var query = this.url + path;
      if (parameters) {
        query += '?';
        var paramsArray = [];
        for (var key in parameters) {
          if (parameters.hasOwnProperty(key)) {
            paramsArray.push([key, '=', parameters[key]].join(''));
          }
        }
        query += paramsArray.join('&');
      }
      var xmlhttp = new XMLHttpRequest();
      xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == XMLHttpRequest.DONE) {
          if (xmlhttp.status == 200) {
            callback(xmlhttp.responseText);
          } else {}
        }
      };
      xmlhttp.open('GET', query, true);
      xmlhttp.send();
    }
  }, {
    key: 'post',

    /**
     * @param path {String}
     * @param parameters {object}
     */
    value: function post(path, parameters) {
      this.xmlhttp = new XMLHttpRequest();
      this.xmlhttp.open('POST', this.url + path, true);
      this.xmlhttp.withCredentials = true;
      this.xmlhttp.setRequestHeader('Content-type', 'application/json');
      this.xmlhttp.onreadystatechange = (function () {
        if (this.xmlhttp.readyState == 4 && this.xmlhttp.status == 200) {}
      }).bind(this);
      this.xmlhttp.send(JSON.stringify(parameters));
    }
  }]);

  return Request;
})();

module.exports = Request;
/** terror */

},{}],4:[function(require,module,exports){
/**
 * Returns the xpath of an element
 * @param element
 * @returns {String} the xpath string
 */
'use strict';

exports.createXPathFromElement = function (element) {
  var allNodes = document.getElementsByTagName('*');
  for (var segs = []; element && element.nodeType == 1; element = element.parentNode) {
    if (element.hasAttribute('id')) {
      var uniqueIdCount = 0;
      for (var n = 0; n < allNodes.length; n++) {
        if (allNodes[n].hasAttribute('id') && allNodes[n].id == element.id) uniqueIdCount++;
        if (uniqueIdCount > 1) break;
      }
      if (uniqueIdCount == 1) {
        segs.unshift('id("' + element.getAttribute('id') + '")');
        return segs.join('/');
      } else {
        segs.unshift(element.localName.toLowerCase() + '[@id="' + element.getAttribute('id') + '"]');
      }
    } else if (element.hasAttribute('class')) {
      segs.unshift(element.localName.toLowerCase() + '[@class="' + element.getAttribute('class') + '"]');
    } else {
      for (var i = 1, sib = element.previousSibling; sib; sib = sib.previousSibling) {
        if (sib.localName == element.localName) i++;
      }
      segs.unshift(element.localName.toLowerCase() + '[' + i + ']');
    }
  }
  return segs.length ? '/' + segs.join('/') : null;
};

/**
 * Get an element by xpath
 * @param path
 * @returns {Node}
 */
exports.lookupElementByXPath = function (path) {
  var evaluator = new XPathEvaluator();
  var result = evaluator.evaluate(path, document.documentElement, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null);
  return result.singleNodeValue;
};

},{}]},{},[1])


//# sourceMappingURL=analytics.js.map