var gulp       = require('gulp');
var fs         = require('fs');
var browserify = require('browserify');
var babelify   = require('babelify');
var gutil      = require('gulp-util');
var uglify     = require('gulp-uglify');
var awspublish = require('gulp-awspublish');
var sourcemaps = require('gulp-sourcemaps');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');

gulp.task('default', function () {

  browserify("./src/index.js", { debug : true, transform: [babelify] })
    .bundle()
    .pipe(source('analytics.js'))
    .pipe(buffer())
    .pipe(sourcemaps.init({ loadMaps: true }))
    .on("error", gutil.log)
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('./dist/'));

  return browserify("./src/index.js", { debug : true, transform: [babelify] })
    .bundle()
    .pipe(source('analytics.min.js'))
    .pipe(buffer())
    .pipe(sourcemaps.init({ loadMaps: true }))
    .pipe(uglify())
    .on("error", gutil.log)
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('./dist/'));

});

gulp.task('publish', ['default'], function() {

  var aws = JSON.parse(fs.readFileSync('./aws.json'));
  var publisher = awspublish.create({
    accessKeyId: aws.key,
    secretAccessKey: aws.secret,
    params: {
      Bucket: 'tmdio.analytics'
    }
  });

  // define custom headers
  var headers = {
    'Cache-Control': 'max-age=315360000, no-transform, public'
  };

  return gulp.src('./dist/*')
    .on("error", gutil.log)
    .pipe(awspublish.gzip())
    .pipe(publisher.publish(headers))
    .pipe(publisher.cache())
    .pipe(awspublish.reporter());
});

gulp.task('watch', ['default'], function () {
  gulp.watch('./src/**/*.js', ['default']);
});